# Python API scraper

We grab data from a JSON API, parse out the information we want, and then sort the data using a custom key.

> based on tutorial by Corey Schafer
