import json

# sortable data 30d | 90d | 365d
ANALYZE_KEY = "30d"

# open package data and load it into a data var
with open("package_info.json", "r") as f:
    data = json.load(f)

# sort our data by ANALYZE_KEY
data.sort(key=lambda o: o["analytics"][ANALYZE_KEY])
print(json.dumps(data, indent=2))
