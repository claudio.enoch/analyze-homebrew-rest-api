import requests
import json
import time

URL = "https://formulae.brew.sh/api/formula.json"
URL_PACKAGE = "https://formulae.brew.sh/api/formula/{}.json"
LIMIT = 5

# get data of all packages
main_r = requests.get(URL)
main_json = main_r.json()
results = []

t1 = time.perf_counter()

# iterate over each package (< LIMIT) and gather data
for package, _ in zip(main_json, range(LIMIT)):
    name = package["name"]
    desc = package["desc"]
    r = requests.get(URL_PACKAGE.format(name))
    r_json = r.json()
    r_str = json.dumps(r_json, indent=2)

    installs_30 = r_json["analytics"]["install_on_request"]["30d"][name]
    installs_90 = r_json["analytics"]["install_on_request"]["90d"][name]
    installs_365 = r_json["analytics"]["install_on_request"]["365d"][name]
    data = {
        "name": name,
        "desc": desc,
        "analytics": {
            "30d": installs_30,
            "90d": installs_90,
            "365d": installs_365
        }
    }
    results.append(data)
    # time.sleep(r.elapsed.total_seconds())
    LIMIT = _ + 1

t2 = time.perf_counter()
print(f"Finished scrapping {LIMIT} packages in {t2-t1} seconds")

# write package analytics to package_info.json file
with open("package_info.json", "w") as f:
    json.dump(results, f, indent=2)
